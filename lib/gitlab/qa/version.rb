# frozen_string_literal: true

module Gitlab
  module QA
    VERSION = '12.4.1'
  end
end
